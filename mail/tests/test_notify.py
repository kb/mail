# -*- encoding: utf-8 -*-
import pytest

from .factories import NotifyFactory


@pytest.mark.django_db
def test_str():
    notify = NotifyFactory(email="test@pkimber.net")
    assert "test@pkimber.net" == str(notify)

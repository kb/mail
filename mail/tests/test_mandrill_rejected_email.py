# -*- encoding: utf-8 -*-
import pytest
import pytz
import responses

from datetime import date, datetime
from http import HTTPStatus

from mail.models import RejectedEmail
from .factories import RejectedEmailFactory


def _test_data():
    return [
        {
            "ts": 1606917662,
            "subject": "Modern Farming",
            "email": "apple@gmail.com",
            "tags": [],
            "state": "rejected",
            "smtp_events": [],
            "subaccount": None,
            "resends": [],
            "reject": {
                "reason": "spam",
                "last_event_at": "2020-02-19 12:29:04.99566",
            },
            "diag": "",
            "bgtools_code": 0,
            "_id": "2d84244a3d964662b1a2c70ddefaff30",
            "sender": "info@kbsoftware.co.uk",
            "template": "candidate-notify-weekly",
            "metadata": {"user_id": "KB"},
            "bounce_description": "general",
            "opens_detail": None,
            "clicks_detail": None,
            "opens": 0,
            "clicks": 0,
        },
        {
            "ts": 1606917662,
            "subject": "Modern Farming",
            "email": "xxxxxxxx@aol.co.uk",
            "tags": [],
            "state": "rejected",
            "smtp_events": [],
            "subaccount": None,
            "resends": [],
            "reject": {
                "reason": "hard-bounce",
                "last_event_at": "2020-11-11 12:57:06.73332",
            },
            "diag": "",
            "bgtools_code": 0,
            "_id": "c01722c95d644dabb30d2cfa9511b774",
            "sender": "info@kbsoftware.co.uk",
            "template": "candidate-notify-weekly",
            "metadata": {"user_id": "KB"},
            "bounce_description": "general",
            "opens_detail": None,
            "clicks_detail": None,
            "opens": 0,
            "clicks": 0,
        },
        {
            "ts": 1625661247,
            "subject": "Modern Farming",
            "email": "ryan123@gmail.com",
            "tags": [],
            "state": "soft-bounced",
            "smtp_events": [],
            "subaccount": None,
            "resends": [],
            "reject": {
                "reason": "soft-bounce",
                "last_event_at": "2020-12-13 12:57:06.733320",
            },
            "diag": "",
            "bgtools_code": 0,
            "_id": "46b3577f85454f65adff0de9ece0558d",
            "sender": "info@kbsoftware.co.uk",
            "template": "candidate-notify-weekly",
            "metadata": {"user_id": "KB"},
            "bounce_description": "general",
            "opens_detail": None,
            "clicks_detail": None,
            "opens": 0,
            "clicks": 0,
        },
    ]


@pytest.mark.django_db
@responses.activate
def test_find_rejected_email_already_exists():
    responses.add(
        responses.POST,
        "https://mandrillapp.com/api/1.0/messages/search.json",
        json=_test_data(),
        status=HTTPStatus.OK,
    )
    # make sure we don't add this record again... (use a different case)
    RejectedEmailFactory(
        email="xxXXXXxx@aol.co.uk",
        event_date=datetime(2020, 11, 11, 12, 57, 6, 733320, tzinfo=pytz.utc),
        template="already-exists",
        reason="already-exists",
    )
    assert (3, 2) == RejectedEmail.objects.find_rejected_email(
        date(2020, 12, 4)
    )
    result = []
    for row in RejectedEmail.objects.all().order_by("email"):
        result.append(
            {
                "email": row.email,
                "event_date": row.event_date,
                "template": row.template,
                "reason": row.reason,
            }
        )
    assert [
        {
            "email": "apple@gmail.com",
            "event_date": datetime(
                2020, 2, 19, 12, 29, 4, 995660, tzinfo=pytz.utc
            ),
            "template": "candidate-notify-weekly",
            "reason": "spam",
        },
        {
            "email": "ryan123@gmail.com",
            "event_date": datetime(
                2020, 12, 13, 12, 57, 6, 733320, tzinfo=pytz.utc
            ),
            "template": "candidate-notify-weekly",
            "reason": "soft-bounce",
        },
        {
            "email": "xxXXXXxx@aol.co.uk",
            "event_date": datetime(
                2020, 11, 11, 12, 57, 6, 733320, tzinfo=pytz.utc
            ),
            "template": "already-exists",
            "reason": "already-exists",
        },
    ] == result


@pytest.mark.django_db
def test_has_clean_history():
    assert RejectedEmail.objects.has_clean_history("orange@gmail.com") is True


@pytest.mark.django_db
def test_has_clean_history_not():
    RejectedEmailFactory(
        email="XXxx@aol.co.uk",
        event_date=datetime(2020, 11, 11, 12, 57, 6, 733320, tzinfo=pytz.utc),
        template="already-exists",
        reason="already-exists",
    )
    assert RejectedEmail.objects.has_clean_history("xxxx@aol.co.uk") is False

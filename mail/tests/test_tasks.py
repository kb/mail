# -*- encoding: utf-8 -*-
import pytest
import responses

from http import HTTPStatus

from mail.tasks import find_rejected_email


def _test_data():
    return [
        {
            "ts": 1606917662,
            "subject": "Modern Farming",
            "email": "apple@gmail.com",
            "tags": [],
            "state": "rejected",
            "smtp_events": [],
            "subaccount": None,
            "resends": [],
            "reject": {
                "reason": "spam",
                "last_event_at": "2020-02-19 12:29:04.99566",
            },
            "diag": "",
            "bgtools_code": 0,
            "_id": "2d84244a3d964662b1a2c70ddefaff30",
            "sender": "info@kbsoftware.co.uk",
            "template": "candidate-notify-weekly",
            "metadata": {"user_id": "KB"},
            "bounce_description": "general",
            "opens_detail": None,
            "clicks_detail": None,
            "opens": 0,
            "clicks": 0,
        },
    ]


@pytest.mark.django_db
@responses.activate
def test_task_find_rejected_email():
    responses.add(
        responses.POST,
        "https://mandrillapp.com/api/1.0/messages/search.json",
        json=_test_data(),
        status=HTTPStatus.OK,
    )
    find_rejected_email()

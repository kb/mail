# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from base.tests.test_utils import PermTestCase
from login.tests.fixture import perm_check
from login.tests.scenario import default_scenario_login
from mail.models import MailTemplate
from mail.tests.factories import NotifyFactory


@pytest.mark.django_db
def test_mail_list(perm_check):
    url = reverse("mail.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_message_create(perm_check):
    url = reverse("mail.message.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_message_list(perm_check):
    url = reverse("mail.message.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_notify_list(perm_check):
    url = reverse("mail.notify.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_notify_create(perm_check):
    url = reverse("mail.notify.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_notify_delete(perm_check):
    notify = NotifyFactory()
    url = reverse("mail.notify.delete", args=[notify.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_template_list(perm_check):
    url = reverse("mail.template.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_rejected_list(perm_check):
    url = reverse("mail.rejected.list")
    perm_check.staff(url)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "template_type",
    [MailTemplate.DJANGO, MailTemplate.MANDRILL, MailTemplate.SPARKPOST],
)
def test_template_create_django(perm_check, settings, template_type):
    settings.MAIL_TEMPLATE_TYPE = template_type
    url = reverse("mail.template.create.django")
    perm_check.staff(url)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "template_type",
    [MailTemplate.DJANGO, MailTemplate.MANDRILL, MailTemplate.SPARKPOST],
)
def test_template_create_mandrill(perm_check, settings, template_type):
    settings.MAIL_TEMPLATE_TYPE = template_type
    url = reverse("mail.template.create.mandrill")
    perm_check.staff(url)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "template_type",
    [MailTemplate.DJANGO, MailTemplate.MANDRILL, MailTemplate.SPARKPOST],
)
def test_template_create_sparkpost(perm_check, settings, template_type):
    settings.MAIL_TEMPLATE_TYPE = template_type
    url = reverse("mail.template.create.sparkpost")
    perm_check.staff(url)


@pytest.mark.django_db
def test_template_update_django(perm_check):
    t = MailTemplate.objects.init_mail_template(
        "hello", "Welcome...", "", False, MailTemplate.DJANGO
    )
    url = reverse("mail.template.update.django", kwargs=dict(pk=t.pk))
    perm_check.staff(url)


@pytest.mark.django_db
def test_template_update_mandrill(perm_check):
    t = MailTemplate.objects.init_mail_template(
        "hello", "Welcome...", "", False, MailTemplate.MANDRILL
    )
    url = reverse("mail.template.update.mandrill", kwargs=dict(pk=t.pk))
    perm_check.staff(url)


@pytest.mark.django_db
def test_template_update_sparkpost(perm_check):
    t = MailTemplate.objects.init_mail_template(
        "hello", "Welcome...", "", False, MailTemplate.SPARKPOST
    )
    url = reverse("mail.template.update.sparkpost", kwargs=dict(pk=t.pk))
    perm_check.staff(url)

# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import datetime
from django.conf import settings
from django.urls import reverse
from django.utils import timezone
from django.utils.http import urlencode
from base.url_utils import url_with_querystring
from http import HTTPStatus
from unittest import mock
from login.tests.factories import TEST_PASSWORD, UserFactory
from mail.models import MailTemplate, Message
from mail.tests.factories import RejectedEmailFactory
from mail.service import (
    queue_mail_message,
    queue_mail_template,
)
from example_mail.tests.model_maker import make_enquiry


@pytest.mark.django_db
def test_mail_list_search(client):
    user = UserFactory(is_staff=True)
    i = 0
    email_address = "ryan@kbsoftware.co.uk"
    while i < 3:
        enquiry = make_enquiry(
            email_address, "test subject", "test description"
        )
        content_data = {
            email_address: {
                "name": "Ryan",
                "title": "Hello",
                "question": enquiry.description,
                "dict": {"age": 52, "colour": "blue"},
            }
        }
        queue_mail_message(
            enquiry,
            [enquiry.email],
            enquiry.subject,
            enquiry.description,
        )
        i += 1
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("mail.list"))
    # check
    assert HTTPStatus.OK == response.status_code
    assert "object_list" in response.context
    assert [
        email_address,
        email_address,
        email_address,
    ] == [x.email for x in response.context["object_list"]]


@pytest.mark.django_db
def test_message_create(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("mail.message.create")
    data = {
        "to_email": "web@pkimber.net",
        "subject": "Apple",
        "message": "Apples and Oranges",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    message = Message.objects.get(subject="Apple")
    assert "Apples and Oranges" == message.description
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert "web@pkimber.net" == mail.email


@pytest.mark.django_db
def test_rejected_list(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("mail.rejected.list"))
    # check
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_rejected_list_search(client):
    user = UserFactory(is_staff=True)
    RejectedEmailFactory(
        email="aaa@pkimber.net",
        event_date=datetime(2014, 10, 1, 6, 0, 0, tzinfo=pytz.utc),
    )
    RejectedEmailFactory(
        email="bbb@pkimber.net",
        event_date=datetime(2014, 10, 30, 6, 0, 0, tzinfo=pytz.utc),
    )
    RejectedEmailFactory(
        email="ccc@pkimber.net",
        event_date=datetime(2014, 10, 15, 6, 0, 0, tzinfo=pytz.utc),
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("mail.rejected.list"))
    # check
    assert HTTPStatus.OK == response.status_code
    assert "object_list" in response.context
    assert [
        "bbb@pkimber.net",
        "ccc@pkimber.net",
        "aaa@pkimber.net",
    ] == [x.email for x in response.context["object_list"]]


@pytest.mark.django_db
def test_rejected_list_search_uppercase(client):
    user = UserFactory(is_staff=True)
    RejectedEmailFactory(
        email="aaa@pkimber.net",
        event_date=datetime(2014, 10, 1, 6, 0, 0, tzinfo=pytz.utc),
    )
    RejectedEmailFactory(
        email="bbb@pkimber.net",
        event_date=datetime(2014, 10, 30, 6, 0, 0, tzinfo=pytz.utc),
    )
    RejectedEmailFactory(
        email="ccc@pkimber.net",
        event_date=datetime(2014, 10, 15, 6, 0, 0, tzinfo=pytz.utc),
    )
    RejectedEmailFactory(
        email="RYAN@kbsoftware.co.uk",
        event_date=datetime(2014, 10, 15, 6, 0, 0, tzinfo=pytz.utc),
    )
    RejectedEmailFactory(
        email="ryant@kbsoftware.co.uk",
        event_date=datetime(2014, 10, 15, 6, 0, 0, tzinfo=pytz.utc),
    )
    url = url_with_querystring(
        reverse("mail.rejected.list"),
        email="Ryan",
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(url)
    # check
    assert HTTPStatus.OK == response.status_code
    assert "object_list" in response.context
    assert ["RYAN@kbsoftware.co.uk", "ryant@kbsoftware.co.uk"] == [
        x.email for x in response.context["object_list"]
    ]


@pytest.mark.django_db
def test_template_create_django(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("mail.template.create.django")
    response = client.post(
        url,
        dict(slug="orange", subject="123", description="ABC", title="Testing"),
    )
    assert HTTPStatus.FOUND == response.status_code
    template = MailTemplate.objects.get(slug="orange")
    assert "ABC" == template.description


@pytest.mark.django_db
def test_template_create_mandrill(client):
    with mock.patch.multiple(
        settings, MAIL_TEMPLATE_TYPE=MailTemplate.MANDRILL
    ):
        user = UserFactory(is_staff=True)
        assert client.login(username=user.username, password=TEST_PASSWORD)
        url = reverse("mail.template.create.mandrill")
        response = client.post(url, dict(slug="apple", title="Testing"))
        assert HTTPStatus.FOUND == response.status_code
        template = MailTemplate.objects.get(slug="apple")
        assert "Testing" == template.title
        assert MailTemplate.MANDRILL == template.template_type


@pytest.mark.django_db
def test_template_create_sparkpost(client):
    with mock.patch.multiple(
        settings, MAIL_TEMPLATE_TYPE=MailTemplate.SPARKPOST
    ):
        user = UserFactory(is_staff=True)
        assert client.login(username=user.username, password=TEST_PASSWORD)
        url = reverse("mail.template.create.sparkpost")
        response = client.post(url, dict(slug="apple", title="Testing"))
        assert HTTPStatus.FOUND == response.status_code
        template = MailTemplate.objects.get(slug="apple")
        assert "Testing" == template.title
        assert MailTemplate.SPARKPOST == template.template_type


@pytest.mark.django_db
def test_template_update_django(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    t = MailTemplate.objects.init_mail_template(
        "hello", "Welcome...", "", False, MailTemplate.DJANGO
    )
    url = reverse("mail.template.update.django", kwargs=dict(pk=t.pk))
    response = client.post(
        url,
        dict(slug="goodbye", subject="123", description="ABC", title="Testing"),
    )
    assert HTTPStatus.FOUND == response.status_code
    with pytest.raises(MailTemplate.DoesNotExist):
        MailTemplate.objects.get(slug="hello")
    template = MailTemplate.objects.get(slug="goodbye")
    assert "ABC" == template.description


@pytest.mark.django_db
def test_template_update_mandrill(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    t = MailTemplate.objects.init_mail_template(
        "pear", "Sorry...", "", False, MailTemplate.MANDRILL
    )
    url = reverse("mail.template.update.mandrill", kwargs=dict(pk=t.pk))
    response = client.post(url, dict(slug="foot", title="Duck"))
    assert HTTPStatus.FOUND == response.status_code
    with pytest.raises(MailTemplate.DoesNotExist):
        MailTemplate.objects.get(slug="pear")
    template = MailTemplate.objects.get(slug="foot")
    assert "Duck" == template.title


@pytest.mark.django_db
def test_template_update_sparkpost(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    t = MailTemplate.objects.init_mail_template(
        "pear", "Sorry...", "", False, MailTemplate.SPARKPOST
    )
    url = reverse("mail.template.update.sparkpost", kwargs=dict(pk=t.pk))
    response = client.post(url, dict(slug="foot", title="Duck"))
    assert HTTPStatus.FOUND == response.status_code
    with pytest.raises(MailTemplate.DoesNotExist):
        MailTemplate.objects.get(slug="pear")
    template = MailTemplate.objects.get(slug="foot")
    assert "Duck" == template.title

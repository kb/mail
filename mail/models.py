# -*- encoding: utf-8 -*-
import json
import humanize
import pytz

from dateutil.parser import parse
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Q
from reversion import revisions as reversion

from base.model_utils import TimeStampedModel


class MailError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class MailTemplateManager(models.Manager):
    def create_mail_template(
        self,
        slug,
        title,
        help_text,
        is_html,
        template_type,
        subject=None,
        description=None,
        is_system=None,
    ):
        obj = self.model(
            title=title,
            slug=slug,
            help_text=help_text,
            is_html=is_html,
            is_system=is_system or False,
            template_type=template_type,
            subject=subject or "",
            description=description or "",
        )
        obj.save()
        return obj

    def init_mail_template(
        self,
        slug,
        title,
        help_text,
        is_html,
        template_type,
        subject=None,
        description=None,
        is_system=None,
    ):
        try:
            obj = self.model.objects.get(slug=slug)
            obj.title = title
            obj.help_text = help_text
            obj.is_html = is_html
            obj.is_system = is_system or False
            obj.template_type = template_type
            # don't overwrite subject if it already exists
            # if not obj.subject:
            obj.subject = subject or ""
            # don't overwrite description if it already exists
            # if not obj.description:
            obj.description = description or ""
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_mail_template(
                slug,
                title,
                help_text,
                is_html,
                template_type,
                subject,
                description,
                is_system,
            )
        return obj


class MailTemplate(TimeStampedModel):
    """email template.

    The 'description' should include details of the available context
    variables.

    If this is a Mandrill template, then we will ignore the description and
    use the slug to lookup the template name using the API.

    If this is a system template, then the user should not be allowed to edit
    it.
    """

    DJANGO = "django"
    MANDRILL = "mandrill"
    SPARKPOST = "sparkpost"

    slug = models.SlugField(unique=True)
    title = models.CharField(max_length=100)
    help_text = models.TextField(blank=True)
    is_html = models.BooleanField(default=False)
    is_system = models.BooleanField(default=False)
    template_type = models.CharField(max_length=32, default=DJANGO)
    subject = models.CharField(max_length=200, blank=True)
    description = models.TextField(blank=True)
    objects = MailTemplateManager()

    class Meta:
        ordering = ("title",)
        verbose_name = "Template"
        verbose_name_plural = "Template"

    @property
    def is_mandrill(self):
        return self.template_type == self.MANDRILL

    @property
    def is_sparkpost(self):
        return self.template_type == self.SPARKPOST

    def __str__(self):
        return "{}".format(self.title)


reversion.register(MailTemplate)


class MessageManager(models.Manager):
    def for_content_object(self, x):
        """All the messages for the 'content_object'.

        23/08/2020, Updated to return *all* matching records (``filter``) rather
        than just one (``get``).

        """
        return self.model.objects.filter(
            content_type=ContentType.objects.get_for_model(x),
            object_id=x.pk,
        )


class Message(TimeStampedModel):
    """the actual mail message - one or more email addresses attached.

    If the template is blank, the subject and description will be sent as
    they are.

    If the template is not blank, the message will be rendered using the
    template.
    """

    subject = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    is_html = models.BooleanField(default=False)
    template = models.ForeignKey(
        MailTemplate, blank=True, null=True, on_delete=models.CASCADE
    )
    # link to the object in the system which asked us to send the email.
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()
    objects = MessageManager()

    class Meta:
        ordering = ["-created"]
        verbose_name = "Mail message"
        verbose_name_plural = "Mail messages"

    def __str__(self):
        return "{}".format(self.subject)

    @property
    def is_mandrill(self):
        return self.template and self.template.is_mandrill

    def attachments(self):
        return self.attachment_set.all()

    @property
    def is_sparkpost(self):
        return self.template and self.template.is_sparkpost


reversion.register(Message)


class Attachment(TimeStampedModel):
    """email attachment.

    Do not use ``reversion`` for this model.  We don't want it keeping track of
    files.

    """

    message = models.ForeignKey(Message, on_delete=models.CASCADE)
    document = models.FileField(upload_to="mail/attachment/")
    use_full_path = models.BooleanField(default=False)
    full_path_file = models.TextField(
        blank=True, help_text="Full path to a file (not using 'FileField')"
    )

    class Meta:
        ordering = ["-created"]
        verbose_name = "Attachment"
        verbose_name_plural = "Attachments"

    def __str__(self):
        return "{}".format(self.message.subject)

    def path_file(self):
        """The file may be a full path / file (not a 'FileField').

        Use either ``document`` or ``full_path_file`` (not both):

        1. Some installations have several Django projects.
        2. The projects share one task queue for sending mail.
        3. We added ``full_path_file`` so attachments created in multiple
           projects can be sent by a single project / queue.

        """
        if self.document:
            return self.document.file.name
        else:
            return self.full_path_file


class MailManager(models.Manager):
    def get_mail_to_send(self):
        rejected_emails = RejectedEmail.objects.all().values_list(
            "email", flat=True
        )
        return (
            self.model.objects.filter(
                Q(sent__isnull=True)
                & (Q(retry_count__lte=10) | Q(retry_count__isnull=True))
            )
            .order_by("pk")
            .exclude(email__in=rejected_emails)
        )

    def emails_for_template(self, date_gt, date_lt, template_slug):
        """List of email addresses sent a template.

        This code originally checked the ``sent`` date, but I think this was
        wrong?  If we were to run the routine twice and the email hadn't yet
        been sent, then it would send another one.

        """
        qs = self.model.objects.filter(
            created__gte=date_gt,
            created__lte=date_lt,
            message__template__slug=template_slug,
        )
        return {x.email for x in qs}


class Mail(TimeStampedModel):
    """email messages to send."""

    message = models.ForeignKey(Message, on_delete=models.CASCADE)
    email = models.EmailField()
    retry_count = models.IntegerField(blank=True, null=True)
    sent = models.DateTimeField(blank=True, null=True)
    sent_response_code = models.CharField(max_length=256, blank=True, null=True)
    objects = MailManager()

    class Meta:
        ordering = ["created"]
        verbose_name = "Mail detail"
        verbose_name_plural = "Mail detail"

    def __str__(self):
        return "{}: {}".format(self.email, self.message.subject)

    def time_to_send(self):
        result = None
        if self.sent:
            result = humanize.naturaldelta(self.sent - self.created)
        return result

    def time_to_send_in_minutes(self):
        result = None
        if self.sent:
            result = int((self.sent - self.created).total_seconds() / 60)
        return result


reversion.register(Mail)


class MailField(models.Model):
    """key, value store for each email."""

    mail = models.ForeignKey(Mail, on_delete=models.CASCADE)
    key = models.CharField(max_length=100)
    is_json = models.BooleanField(default=False)
    value = models.TextField()

    class Meta:
        ordering = ["mail", "key"]
        verbose_name = "Mail field"
        verbose_name_plural = "Mail fields"

    def __str__(self):
        return "{}: {}".format(self.mail.email, self.key)

    @property
    def data(self):
        """Return the value of the mail field.

        - return a string if ``is_json`` is ``False``
        - return a ``dict`` if ``is_json`` is ``True``

        """
        if self.is_json:
            return json.loads(self.value)

        else:
            return self.value


reversion.register(MailField)


class NotifyManager(models.Manager):
    def create_notify(self, email):
        obj = self.model(email=email)
        obj.save()
        return obj


class Notify(TimeStampedModel):
    """List of people to notify on an event e.g. enquiry or payment."""

    email = models.EmailField()
    objects = NotifyManager()

    class Meta:
        verbose_name = "Notify Address"
        verbose_name_plural = "Notify Addresses"

    def __str__(self):
        return "{}".format(self.email)


reversion.register(Notify)


class RejectedEmailManager(models.Manager):
    def find_rejected_email(self, for_date):
        """Search for rejected Mandrill messages for a day and mark as rejected.

        Uses the model to record the rejection.

        How to Search Outbound Activity in Transactional Email
        https://mandrill.zendesk.com/hc/en-us/articles/360039298733-How-to-Search-Outbound-Activity-in-Transactional-Email

        state
          Search by the status of the email.
          Options are 'sent', 'bounced', 'soft-bounced', 'rejected', 'spam', and 'unsub'

        """
        import mandrill

        count = 0
        client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
        query = dict(
            query="state:bounced OR state:rejected OR state:soft-bounced",
            date_from=for_date.strftime("%Y-%m-%d 00:00:00"),
            date_to=for_date.strftime("%Y-%m-%d 23:59:59"),
            limit=1000,
        )
        data = client.messages.search(**query)
        for row in data:
            email = row["email"]
            template = row["template"]
            reject = row.get("reject")
            if reject:
                last_event_at = reject["last_event_at"]
                event_date = pytz.utc.localize(parse(last_event_at))
                reason = reject["reason"]
            else:
                last_event_at = row["@timestamp"]
                event_date = parse(last_event_at)
                reason = row["state"]
            if RejectedEmail.objects.init_rejected_mailing(
                email,
                event_date,
                template,
                reason,
            ):
                count = count + 1
        return len(data), count

    def has_clean_history(self, email):
        qs = self.model.objects.filter(email__iexact=email)
        return not qs.exists()

    def init_rejected_mailing(self, email, event_date, template, reason):
        result = None
        try:
            self.model.objects.get(email__iexact=email, event_date=event_date)
        except self.model.DoesNotExist:
            result = self.model(
                email=email,
                event_date=event_date,
                template=template,
                reason=reason,
            )
            result.save()
        return result


class RejectedEmail(TimeStampedModel):
    """email addresses rejected (by Mandrill etc).

    - The ``last_event_at`` is *the timestamp of the most recent event that
      either created or renewed this rejection*.

    """

    email = models.EmailField()
    # from the Mandrill ``last_event_at`` field
    event_date = models.DateTimeField()
    # same length as the ``slug`` in the ``mail.MailTemplate`` model
    template = models.CharField(max_length=50)
    reason = models.CharField(max_length=50)
    objects = RejectedEmailManager()

    class Meta:
        ordering = ("-pk",)
        verbose_name = "Mandrill Audit"

    def __str__(self):
        return "{} ({}): {} - {}".format(
            self.pk,
            self.created.strftime("%d/%m/%Y %H:%M"),
            self.email,
            self.reason,
        )

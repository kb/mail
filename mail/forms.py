# -*- encoding: utf-8 -*-
from django import forms

from base.form_utils import RequiredFieldForm

from .models import MailTemplate, Notify


class MessageForm(forms.Form):
    to_email = forms.EmailField()
    subject = forms.CharField()
    message = forms.CharField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in self.fields:
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})


class MailTemplateCreateDjangoForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("subject", "description", "title"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = MailTemplate
        fields = ("slug", "title", "subject", "description")


class MailTemplateCreateRemoteForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("title",):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = MailTemplate
        fields = ("slug", "title")


class MailTemplateUpdateDjangoForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("subject", "description", "title"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = MailTemplate
        fields = ("slug", "title", "subject", "description")


class MailTemplateUpdateRemoteForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("title",):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = MailTemplate
        fields = ("slug", "title")


class NotifyEmptyForm(forms.ModelForm):
    class Meta:
        model = Notify
        fields = ()


class NotifyForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("email",):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = Notify
        fields = ("email",)

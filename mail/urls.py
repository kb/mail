# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import (
    MailListView,
    MailTemplateCreateDjangoView,
    MailTemplateCreateRemoteView,
    MailTemplateListView,
    MailTemplateUpdateDjangoView,
    MailTemplateUpdateRemoteView,
    MessageCreateView,
    MessageListView,
    NotifyCreateView,
    NotifyDeleteView,
    NotifyListView,
    RejectedEmailListView,
)


urlpatterns = [
    re_path(r"^$", view=MessageListView.as_view(), name="mail.message.list"),
    re_path(
        r"^create/$",
        view=MessageCreateView.as_view(),
        name="mail.message.create",
    ),
    re_path(
        r"^mail/$",
        view=MailListView.as_view(),
        name="mail.list",
    ),
    re_path(
        r"^notify/$",
        view=NotifyListView.as_view(),
        name="mail.notify.list",
    ),
    re_path(
        r"^notify/create/$",
        view=NotifyCreateView.as_view(),
        name="mail.notify.create",
    ),
    re_path(
        r"^notify/(?P<pk>\d+)/delete/$",
        view=NotifyDeleteView.as_view(),
        name="mail.notify.delete",
    ),
    re_path(
        r"^rejected/$",
        view=RejectedEmailListView.as_view(),
        name="mail.rejected.list",
    ),
    re_path(
        r"^template/$",
        view=MailTemplateListView.as_view(),
        name="mail.template.list",
    ),
    re_path(
        r"^template/create/django/$",
        view=MailTemplateCreateDjangoView.as_view(),
        name="mail.template.create.django",
    ),
    re_path(
        r"^template/create/mandrill/$",
        view=MailTemplateCreateRemoteView.as_view(),
        name="mail.template.create.mandrill",
    ),
    re_path(
        r"^template/create/sparkpost/$",
        view=MailTemplateCreateRemoteView.as_view(),
        name="mail.template.create.sparkpost",
    ),
    re_path(
        r"^template/(?P<pk>\d+)/update/django/$",
        view=MailTemplateUpdateDjangoView.as_view(),
        name="mail.template.update.django",
    ),
    re_path(
        r"^template/(?P<pk>\d+)/update/mandrill/$",
        view=MailTemplateUpdateRemoteView.as_view(),
        name="mail.template.update.mandrill",
    ),
    re_path(
        r"^template/(?P<pk>\d+)/update/sparkpost/$",
        view=MailTemplateUpdateRemoteView.as_view(),
        name="mail.template.update.sparkpost",
    ),
]

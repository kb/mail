# -*- encoding: utf-8 -*-
from datetime import date
from dateutil.relativedelta import relativedelta
from django.core.management.base import BaseCommand

from mail.models import RejectedEmail


class Command(BaseCommand):
    help = "Mandrill - find rejected messages #5388"

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        for_date = date.today()
        self.stdout.write("- {}".format(for_date.strftime("%a %d/%m/%Y")))
        total, count = RejectedEmail.objects.find_rejected_email(for_date)
        self.stdout.write(
            "Complete (found {} updated {}): {}".format(total, count, self.help)
        )

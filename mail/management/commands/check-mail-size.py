# -*- encoding: utf-8 -*-
import humanize
import pathlib

from django.core.management.base import BaseCommand, CommandError

from mail.models import Mail, MailError
from mail.service import file_size_as_base64


class Command(BaseCommand):
    help = "mail attachments - check file size in base64 #5863"

    def add_arguments(self, parser):
        parser.add_argument("mail_pk", type=int)

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        mail_pk = options["mail_pk"]
        mail = Mail.objects.get(pk=mail_pk)
        self.stdout.write(
            "mail: {} sent: {} retry_count: {}".format(
                mail.pk, mail.sent, mail.retry_count
            )
        )
        total = total_base64 = 0
        for attachment in mail.message.attachments():
            path_file = attachment.path_file()
            path = pathlib.Path(path_file)
            if path.exists():
                size = path.stat().st_size
                total = total + size
                size_base64 = file_size_as_base64(path_file)
                total_base64 = total_base64 + size_base64
                self.stdout.write(
                    "{:<10} == {:<10} (base64):  {}".format(
                        size, size_base64, path.name
                    )
                )
            else:
                raise MailError(
                    "Attachment does not exist: {}".format(path_file)
                )
        self.stdout.write(
            "{:<10} == {:<10} (base64)".format(total, total_base64)
        )
        # to save the email, so we can try sending it again!
        if mail.retry_count and mail.retry_count >= 10:
            mail.retry_count = 0
            mail.save()
        self.stdout.write("{} - Complete".format(self.help))

# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from mail.models import RejectedEmail


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("email", type=str)

    def handle(self, *args, **options):
        email = options["email"]
        self.stdout.write(
            "{} has_clean_history: {}".format(
                email, RejectedEmail.objects.has_clean_history(email)
            )
        )

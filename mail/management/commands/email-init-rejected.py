# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.utils import timezone

from mail.models import RejectedEmail


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("email", type=str)
        parser.add_argument(
            "reason",
            type=str,
            help=(
                "Reason for rejection "
                "e.g. 'hard-bounce', 'soft-bounce' or 'spam'"
            ),
        )
        parser.add_argument(
            "template", type=str, help="candidate-notify-weekly"
        )

    def handle(self, *args, **options):
        email = options["email"]
        if RejectedEmail.objects.has_clean_history(email):
            reason = options["reason"]
            template = options["template"]
            self.stdout.write(
                "init_rejected_mailing for {} ({})".format(email, reason)
            )
            RejectedEmail.objects.init_rejected_mailing(
                email, timezone.now(), template, reason
            )
            self.stdout.write(
                "{} has_clean_history: {}".format(
                    email, RejectedEmail.objects.has_clean_history(email)
                )
            )
        else:
            self.stdout.write("{} has already been rejected:".format(email))
            qs = RejectedEmail.objects.filter(email__iexact=email)
            self.stdout.write(
                "  {:<17} {:<30} {:<15} {}".format(
                    "created",
                    "email",
                    "reason",
                    "template",
                )
            )
            for x in qs:
                self.stdout.write(
                    "- {:<17} {:<30} {:<15} {}".format(
                        x.created.strftime("%d/%m/%Y %H:%M"),
                        x.email,
                        x.reason,
                        x.template,
                    )
                )

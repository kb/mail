# -*- encoding: utf-8 -*-
import django_filters

from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.conf import settings
from django.contrib import messages
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.views.generic import CreateView, FormView, ListView, UpdateView
from django_filters.views import FilterView

from base.view_utils import BaseMixin
from django.views.generic.edit import DeleteView
from mail.forms import (
    MailTemplateCreateDjangoForm,
    MailTemplateCreateRemoteForm,
    MailTemplateUpdateDjangoForm,
    MailTemplateUpdateRemoteForm,
    MessageForm,
    NotifyEmptyForm,
    NotifyForm,
)
from mail.models import Mail, Message, MailTemplate, Notify, RejectedEmail
from mail.service import queue_mail_message
from mail.tasks import process_mail


def _get_remote_template_type():
    if settings.MAIL_TEMPLATE_TYPE:
        template_type = settings.MAIL_TEMPLATE_TYPE
    elif settings.SPARKPOST_API_KEY:
        template_type = MailTemplate.SPARKPOST
    elif settings.MANDRILL_API_KEY:
        template_type = MailTemplate.MANDRILL
    else:
        template_type = MailTemplate.DJANGO
    return template_type


class MailListFilter(django_filters.FilterSet):
    email = django_filters.CharFilter(lookup_expr="iexact")

    class Meta:
        model = Mail
        fields = ["email"]


class MailListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, FilterView
):
    filterset_class = MailListFilter
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        get_parameters = self.request.GET.copy()
        if self.page_kwarg in get_parameters:
            del get_parameters[self.page_kwarg]
        email = self.request.GET.get("email")
        qs = (
            RejectedEmail.objects.all()
            .filter(email__iexact=email)
            .order_by("-event_date")
        )
        context.update(
            dict(get_parameters=get_parameters.urlencode(), rejected_emails=qs)
        )
        return context

    def get_queryset(self):
        return Mail.objects.all().order_by("-created")


class MailTemplateCreateDjangoView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = MailTemplateCreateDjangoForm
    model = MailTemplate
    template_name = "mail/mailtemplate_create_django_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.template_type = MailTemplate.DJANGO
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("mail.template.list")


class MailTemplateCreateRemoteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = MailTemplateCreateRemoteForm
    model = MailTemplate
    template_name = "mail/mailtemplate_create_remote_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.template_type = _get_remote_template_type()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        template_type = _get_remote_template_type()
        context.update({"template_type": template_type})
        return context

    def get_success_url(self):
        return reverse("mail.template.list")


class MailTemplateListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = MailTemplate

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        template_type = _get_remote_template_type()
        context.update({"template_type": template_type})
        return context


class MailTemplateUpdateDjangoView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = MailTemplateUpdateDjangoForm
    model = MailTemplate
    template_name = "mail/mailtemplate_update_django_form.html"

    def get_success_url(self):
        return reverse("mail.template.list")


class MailTemplateUpdateRemoteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = MailTemplateUpdateRemoteForm
    model = MailTemplate
    template_name = "mail/mailtemplate_update_remote_form.html"

    def get_success_url(self):
        return reverse("mail.template.list")


class MessageCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, FormView
):
    """Send a test mail message."""

    form_class = MessageForm
    template_name = "mail/message_create_form.html"

    def form_valid(self, form):
        to_email = form.cleaned_data["to_email"]
        subject = form.cleaned_data["subject"]
        description = form.cleaned_data["message"]
        with transaction.atomic():
            queue_mail_message(
                self.request.user, [to_email], subject, description
            )
            transaction.on_commit(lambda: process_mail.send())
            message = "Test Mail sent to '{}'".format(to_email)
            messages.info(self.request, message)
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict())
        return context

    def get_initial(self):
        result = super().get_initial()
        time_now = timezone.now().strftime("%d/%m/%Y %H:%M")
        url = self.request.build_absolute_uri()
        result.update(
            dict(
                to_email=self.request.user.email,
                subject="Test email ({}) ".format(time_now),
                message="From {}".format(url),
            )
        )
        return result

    def get_success_url(self):
        return reverse("mail.message.create")


class MessageListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = Message
    paginate_by = 10


class NotifyListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = Notify


class NotifyCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = NotifyForm
    model = Notify

    def get_success_url(self):
        return reverse("mail.notify.list")


class NotifyDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, DeleteView
):
    form_class = NotifyEmptyForm
    template_name = "mail/notify_delete_form.html"
    model = Notify

    def get_success_url(self):
        return reverse("mail.notify.list")


class RejectedEmailFilter(django_filters.FilterSet):
    email = django_filters.CharFilter(lookup_expr="icontains")

    class Meta:
        model = RejectedEmail
        fields = ["email"]


class RejectedEmailListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, FilterView
):
    filterset_class = RejectedEmailFilter
    paginate_by = 20

    def get_queryset(self):
        return RejectedEmail.objects.all().order_by("-event_date")

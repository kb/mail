# -*- encoding: utf-8 -*-
import dramatiq
import logging

from datetime import date
from dateutil.relativedelta import relativedelta
from django.conf import settings

from .models import RejectedEmail
from .service import send_mail


logger = logging.getLogger(__name__)


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def find_rejected_email():
    for_date = date.today() + relativedelta(days=-1)
    for_date_as_str = for_date.strftime("%a %d/%m/%Y")
    logger.info("find_rejected_email for {}".format(for_date_as_str))
    total, count = RejectedEmail.objects.find_rejected_email(for_date)
    logger.info(
        "find_rejected_email for {}. Found {} Updated {}".format(
            for_date_as_str, total, count
        )
    )
    return count


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def process_mail():
    result = send_mail()
    count = len(result)
    if result:
        logger.info("Process mail - {} records".format(count))
    return count


def schedule_find_rejected_email():
    """APScheduler would like to find rejected email messages.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    find_rejected_email.send()


def schedule_process_mail():
    """APScheduler would like to process the email.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    https://www.kbsoftware.co.uk/crm/ticket/5001/

    """
    process_mail.send()

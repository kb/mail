#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u

if [[ -z "${DATABASE_HOST}" ]]; then
  echo Drop database: $DATABASE_NAME
  psql -U $DATABASE_USER -c "DROP DATABASE IF EXISTS $DATABASE_NAME;"
  echo Create database: $DATABASE_NAME
  psql -U $DATABASE_USER -c "CREATE DATABASE $DATABASE_NAME TEMPLATE=template0 ENCODING='utf-8';"
else
  echo Drop database: $DATABASE_NAME
  PGPASSWORD=$DATABASE_PASS psql --host $DATABASE_HOST --port $DATABASE_PORT -U $DATABASE_USER -c "DROP DATABASE IF EXISTS $DATABASE_NAME;"
  echo Create database: $DATABASE_NAME
  PGPASSWORD=$DATABASE_PASS psql --host $DATABASE_HOST --port $DATABASE_PORT -U $DATABASE_USER -c "CREATE DATABASE $DATABASE_NAME TEMPLATE=template0 ENCODING='utf-8';"
fi

django-admin migrate --noinput
django-admin demo-data-login
django-admin init_app_mail
django-admin demo_data_mail
django-admin runserver

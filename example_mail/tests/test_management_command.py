# -*- encoding: utf-8 -*-
import os
import pytest

from django.core.management import call_command

from example_mail.tests.factories import EnquiryFactory
from mail.models import Attachment, Mail
from mail.service import queue_mail_message


@pytest.mark.django_db
def test_check_mail_size():
    assert 0 == Mail.objects.count()
    assert 0 == Attachment.objects.count()
    file_name = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "sample.odt"
    )
    message = queue_mail_message(
        EnquiryFactory(),
        ["web@pkimber.net"],
        "Cake",
        "Favourite cakes",
        attachments=[file_name],
        use_full_path=True,
    )
    assert 1 == Attachment.objects.count()
    assert 1 == Mail.objects.count()
    mail = Mail.objects.first()
    call_command("mail_send")
    call_command("check-mail-size", mail.pk)


@pytest.mark.django_db
def test_demo_data():
    call_command("init_app_mail")
    call_command("demo_data_mail")

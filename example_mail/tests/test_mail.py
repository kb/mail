# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import datetime
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from freezegun import freeze_time

from mail.models import Mail
from mail.tests.factories import (
    MailTemplateFactory,
    MailFactory,
    MessageFactory,
    RejectedEmailFactory,
)
from .factories import EnquiryFactory


@pytest.mark.django_db
def test_email_for_template():
    """Which email address has received the template."""
    template = MailTemplateFactory(slug="orange")
    message = MessageFactory(content_object=EnquiryFactory(), template=template)
    MailFactory(email="web@pkimber.net", message=message)
    date_gt = timezone.now() + relativedelta(days=-2)
    date_lt = timezone.now() + relativedelta(days=+2)
    assert set(["web@pkimber.net"]) == Mail.objects.emails_for_template(
        date_gt, date_lt, "orange"
    )


@pytest.mark.django_db
def test_email_for_template_exclude_duplicate():
    """Do not count emails which have not been sent."""
    template = MailTemplateFactory(slug="orange")
    message = MessageFactory(content_object=EnquiryFactory(), template=template)
    MailFactory(email="one@pkimber.net", message=message)
    MailFactory(email="two@pkimber.net", message=message)
    MailFactory(email="one@pkimber.net", message=message)
    date_gt = timezone.now() + relativedelta(days=-2)
    date_lt = timezone.now() + relativedelta(days=+2)
    assert set(
        ["one@pkimber.net", "two@pkimber.net"]
    ) == Mail.objects.emails_for_template(date_gt, date_lt, "orange")


@pytest.mark.django_db
def test_email_for_template_no_template():
    template = MailTemplateFactory(slug="orange")
    msg_1 = MessageFactory(content_object=EnquiryFactory(), template=template)
    msg_2 = MessageFactory(content_object=EnquiryFactory(), template=None)
    MailFactory(email="one@pkimber.net", message=msg_1)
    MailFactory(email="two@pkimber.net", message=msg_2)
    MailFactory(email="six@pkimber.net", message=msg_1)
    date_gt = timezone.now() + relativedelta(days=-2)
    date_lt = timezone.now() + relativedelta(days=+2)
    assert set(
        ["one@pkimber.net", "six@pkimber.net"]
    ) == Mail.objects.emails_for_template(date_gt, date_lt, "orange")


@pytest.mark.django_db
def test_email_for_template_slug():
    template = MailTemplateFactory(slug="orange")
    msg_1 = MessageFactory(content_object=EnquiryFactory(), template=template)
    template = MailTemplateFactory(slug="apple")
    msg_2 = MessageFactory(content_object=EnquiryFactory(), template=template)
    MailFactory(email="one@pkimber.net", message=msg_1)
    MailFactory(email="two@pkimber.net", message=msg_1)
    MailFactory(email="six@pkimber.net", message=msg_2)
    date_gt = timezone.now() + relativedelta(days=-2)
    date_lt = timezone.now() + relativedelta(days=+2)
    assert set(
        ["one@pkimber.net", "two@pkimber.net"]
    ) == Mail.objects.emails_for_template(date_gt, date_lt, "orange")


@pytest.mark.django_db
def test_get_mail_to_send():
    message = MessageFactory(content_object=EnquiryFactory())
    mail_1 = MailFactory(message=message, retry_count=None, sent=None)
    mail_2 = MailFactory(message=message, retry_count=None)
    mail_3 = MailFactory(message=message, retry_count=10)
    mail_4 = MailFactory(message=message, retry_count=15)
    mail_5 = MailFactory(message=message, retry_count=None, sent=timezone.now())
    mail_6 = MailFactory(message=message, retry_count=10, sent=timezone.now())
    mail_7 = MailFactory(
        email="web@pkimber.net", message=message, retry_count=None
    )
    RejectedEmailFactory(email="web@pkimber.net", event_date=timezone.now())
    result = Mail.objects.get_mail_to_send()
    assert [mail_1.pk, mail_2.pk, mail_3.pk] == [x.pk for x in result]


@pytest.mark.django_db
def test_time_to_send():
    message = MessageFactory(content_object=EnquiryFactory())
    with freeze_time(datetime(2020, 1, 1, 1, 1, 1, tzinfo=pytz.utc)):
        mail = MailFactory(
            message=message, sent=datetime(2020, 1, 1, 2, 1, 1, tzinfo=pytz.utc)
        )
    assert "an hour" == mail.time_to_send()


@pytest.mark.django_db
def test_time_to_send_in_minutes():
    message = MessageFactory(content_object=EnquiryFactory())
    with freeze_time(datetime(2020, 1, 1, 1, 1, 1, tzinfo=pytz.utc)):
        mail = MailFactory(
            message=message, sent=datetime(2020, 1, 1, 2, 1, 1, tzinfo=pytz.utc)
        )
    assert 60 == mail.time_to_send_in_minutes()

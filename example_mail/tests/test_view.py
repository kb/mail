# -*- encoding: utf-8 -*-
from django.test import TestCase
from django.urls import reverse


class TestView(TestCase):
    def test_project_home(self):
        response = self.client.get(reverse("project.home"))
        self.assertEqual(response.status_code, 200)

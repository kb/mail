# -*- encoding: utf-8 -*-
import pytest

from .factories import EnquiryFactory

from example_mail.base import get_env_variable
from mail.service import queue_mail_message
from mail.tasks import process_mail, schedule_process_mail


@pytest.mark.django_db
def test_process_mail():
    email = get_env_variable("TEST_EMAIL_ADDRESS_1")
    enquiry = EnquiryFactory(email=email, subject="Fruit", description="Apple")
    queue_mail_message(
        enquiry, [enquiry.email], enquiry.subject, enquiry.description
    )
    assert 1 == process_mail()


@pytest.mark.django_db
def test_schedule_process_mail():
    email = get_env_variable("TEST_EMAIL_ADDRESS_1")
    enquiry = EnquiryFactory(email=email, subject="Fruit", description="Apple")
    queue_mail_message(
        enquiry, [enquiry.email], enquiry.subject, enquiry.description
    )
    schedule_process_mail()

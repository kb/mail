# -*- encoding: utf-8 -*-
import pathlib
import pytest

from django.conf import settings

from mail.tests.factories import AttachmentFactory, MessageFactory
from .factories import EnquiryFactory


@pytest.mark.django_db
def test_path_file():
    x = AttachmentFactory(
        message=MessageFactory(subject="abc", content_object=EnquiryFactory())
    )
    assert x.document.file.name == x.path_file()
    assert (
        str(
            pathlib.Path(
                settings.BASE_DIR,
                settings.MEDIA_ROOT,
                "mail",
                "attachment",
                pathlib.Path(x.document.file.name).name,
            )
        )
        in x.path_file()
    )


@pytest.mark.django_db
def test_path_file_full_path_name():
    x = AttachmentFactory(
        message=MessageFactory(subject="abc", content_object=EnquiryFactory()),
        document=None,
        full_path_file="/home/patrick/Downloads/grass.rst",
    )
    assert "/home/patrick/Downloads/grass.rst" == x.path_file()


@pytest.mark.django_db
def test_str():
    message = MessageFactory(subject="abc", content_object=EnquiryFactory())
    x = AttachmentFactory(message=message)
    assert "abc" == str(x)

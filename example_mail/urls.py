# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, re_path, reverse_lazy
from django.views.generic import RedirectView

from .views import HomeView, EnquiryCreateView, EnquiryListView, SettingsView


urlpatterns = [
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^", view=include("login.urls")),
    re_path(r"^mail/", view=include("mail.urls")),
    re_path(
        r"^enquiry/$",
        view=EnquiryListView.as_view(),
        name="example.enquiry.list",
    ),
    re_path(
        r"^enquiry/create/$",
        view=EnquiryCreateView.as_view(),
        name="example.enquiry.create",
    ),
    re_path(
        r"^dash/$",
        view=RedirectView.as_view(url=reverse_lazy("project.home")),
        name="project.dash",
    ),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#   ^ helper function to return a URL pattern for serving files in debug mode.
# https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user

urlpatterns += staticfiles_urlpatterns()
if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        re_path(r"^__debug__/", include(debug_toolbar.urls)),
    ]

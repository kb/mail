# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

# from example_mail.tests.scenario import default_scenario_mail


class Command(BaseCommand):
    help = "Create demo data for 'mail'"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        # default_scenario_mail()
        self.stdout.write(
            "'demo_data_mail' no longer runs "
            "'default_scenario_mail'".format(self.help)
        )
        self.stdout.write("{} - Complete".format(self.help))

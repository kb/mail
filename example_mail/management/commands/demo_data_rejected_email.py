# -*- encoding: utf-8 -*-
import pytz

from django.core.management.base import BaseCommand
from faker import Faker

from mail.models import RejectedEmail

# from example_mail.tests.scenario import default_scenario_mail


class Command(BaseCommand):
    help = "Create demo data for 'RejectedEmail'"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        RejectedEmail.objects.all().delete()
        fake = Faker()
        i = 1
        while i < 50:
            RejectedEmail.objects.init_rejected_mailing(
                template=fake.text(max_nb_chars=10).lower(),
                email=fake.email(),
                event_date=fake.date_time(tzinfo=pytz.utc),
                reason=fake.text(max_nb_chars=50),
            )
            i += 1
        self.stdout.write("{} - Complete".format(self.help))

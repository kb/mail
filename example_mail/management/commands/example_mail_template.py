# -*- encoding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.utils import timezone

from mail.models import Mail, MailTemplate
from mail.service import queue_mail_template
from mail.tasks import process_mail


class Command(BaseCommand):
    help = "Example send mail template #4403"
    EXAMPLE_MAIL_TEMPLATE = "example_mail_template"

    def init_template(self):
        MailTemplate.objects.init_mail_template(
            self.EXAMPLE_MAIL_TEMPLATE,
            "Example Mail Template",
            (
                "You can add the following variables to the template:\n"
                "{{ EXAMPLE }} an example variable name."
            ),
            False,
            MailTemplate.MANDRILL,
        )

    def send_email(self, content_object):
        url_a = (
            '<a href="https://www.bbc.co.uk/news" mc:disable-tracking>News</a>'
        )
        url_b = '<a href="https://www.bbc.co.uk/sport" mc:disable-tracking>Sport</a>'
        queue_mail_template(
            content_object,
            self.EXAMPLE_MAIL_TEMPLATE,
            context={
                "test@pkimber.net": {
                    "DATE": timezone.now().strftime("%d-%b-%Y %H:%M:%S"),
                    "ITEMS": "<br />A. {}<br />B. {}<br />".format(
                        url_a, url_b
                    ),
                    "NAME": "Granny Smith",
                    "DESCRIPTION": "Apples",
                    "TOTAL": "123.34",
                    "UNSUB": "https://www.kbsoftware.co.uk/",
                }
            },
        )

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        # Mark existing emails as sent already!!
        Mail.objects.filter(sent__isnull=True).update(sent=timezone.now())
        self.init_template()
        user = get_user_model().objects.get(username="admin")
        self.send_email(user)
        process_mail.send()
        self.stdout.write("{} - Complete".format(self.help))

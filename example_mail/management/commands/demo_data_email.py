# -*- encoding: utf-8 -*-
import pytz
from mail.service import (
    queue_mail_message,
    queue_mail_template,
)
from example_mail.tests.model_maker import make_enquiry
from django.core.management.base import BaseCommand
from faker import Faker
import time
from mail.models import RejectedEmail
from mail.models import Mail

# from example_mail.tests.scenario import default_scenario_mail


class Command(BaseCommand):
    help = "Create demo data for 'Email'"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        Mail.objects.all().delete()
        fake = Faker()
        i = 1
        while i < 5:
            h = 0
            email_address = fake.email()
            while h < 2:
                enquiry = make_enquiry(
                    email_address,
                    fake.text(max_nb_chars=25),
                    fake.text(max_nb_chars=50),
                )
                content_data = {
                    email_address: {
                        "name": fake.text(max_nb_chars=8),
                        "title": fake.text(max_nb_chars=8),
                        "question": enquiry.description,
                        "dict": {"age": 52, "colour": "blue"}
                        # "list": [1, 3, 9],
                    }
                }
                queue_mail_message(
                    enquiry,
                    [enquiry.email],
                    enquiry.subject,
                    enquiry.description,
                )
                RejectedEmail.objects.init_rejected_mailing(
                    template=fake.text(max_nb_chars=10).lower(),
                    email=email_address,
                    event_date=fake.date_time(tzinfo=pytz.utc),
                    reason=fake.text(max_nb_chars=50),
                )
                h += 1
                time.sleep(1)
            i += 1
        self.stdout.write("{} - Complete".format(self.help))
